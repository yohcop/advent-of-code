##
## Solves the example of problem number 1
##
## Solving the whole problem would require loading a file in memory.
##
.globl __start

.rodata
  msg1: .string "1abc2\npqr3stu8vwx\na1b2c3d4e5f\ntreb7uchet"

.text

__start:
  li a0, 4  # printString
  la a1, msg1
  ecall

  # s1 is position in the string
  la s1, msg1
  li s2, 0  # s2 is our Counter
  li s3, 10  # const s3 = 10 (for comparison)
  li s4, '\n'  # const s4 = new line, for end of line
  li s5, '0'  # const s5 = '0', for comparing to ASCII chars
  li s6, ':' # const s6 = ':', which comes after `9`

  li a1, '\n'
  call char

  j first_digit

# Prints a char in a1.
# NOTE: using a "call" modifies t1, which is why I am avoiding it.
char:  # Do this before: li a1, '\n'
  li a0, 11
  ecall
  li a1, '\n'
  ecall
  jr ra

int:  # Do this before: mv a1, t0
  li a0, 1
  ecall
  li a0, 11
  li a1, '\n'
  ecall
  jr ra

first_digit:
  # In here:
  # t0 is the current character, then replaced with the ASCII number
  li a1, '>'
  call char

  lb t0, 0(s1)  # t0 = current character @ s1
  # Print the character.
  # mv a1, t0
  # call char

  beq t0, x0, end # end of file
  bge t0, s6, next_first # branch if greater than '9'.
  blt t0, s4, next_first # branch if lower than '0'

  # This is a number! Add it to our 
  addi t0, t0, -48  # t0 = t0 - '0'
  mul t0, t0, s3  # Multiply this number by 10

  mv a1, t0  # Print the number.
  call int

  add s2, s2, t0
  # we do not advance our position, since we may want to use
  # that same number as "last" digit.
  j last_digit
  
  next_first:
  addi s1, s1, 1

  j first_digit
  
last_digit:
  # In here:
  # t0 is the current character, then replaced with the ASCII number
  # t3 is the last digit found
  li a1, '<'
  call char

  lb t0, 0(s1)  # t0 = current character @ s1
  # Print the character
  # mv a1, t0
  # call char

  beq t0, s4, eol # end of line
  beq t0, x0, eol # end of file
  bge t0, s6, next_last # branch if greater than 10.
  blt t0, s4, next_last # branch if lower than 0

  # We found a digit.
  addi t3, t0, -48  # t3 = t0 - '0'
  addi s1, s1, 1  # Move the pointer to the next digit.
  j last_digit
  
next_last:
  addi s1, s1, 1  # Move the pointer to the next digit.
  j last_digit

eol:
  li a1, '!'
  call char
  mv a1, t3
  call int
  add s2, s2, t3  # Add last digit found
  addi s1, s1, 1  # Move the pointer to the next digit.

  beq t0, x0, end # end of file

  j first_digit

end:
  li a0, 11  # ecall code
  li a1, '=' # character to print
  ecall

  li a0, 1 # print int
  mv a1, s2
  ecall

exit:
  li a0, 10
  ecall  # return 0