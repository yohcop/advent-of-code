# advent-of-code

## For RISC-V assembly

### Install jupiter, a RISC-V assembler and simulator

```
wget https://github.com/andrescv/Jupiter/releases/download/v3.1/Jupiter-3.1-linux.zip
unzip Jupiter-3.1-linux.zip
```

### Run a solution:
```
image/bin/jupiter src/2023/01/01.s
```

## For x86 assembly

### Install nasm

Note: I need 2.16 for debugging correctly with GDB.

```
sudo apt install alien
wget https://www.nasm.us/pub/nasm/releasebuilds/2.16.01/linux/nasm-2.16.01-0.fc36.x86_64.rp
sudo alien -i nasm-2.16.01-0.fc36.x86_64.rpm
```

```
nasm -f elf64 src/2023/02/02.asm -o build/02.o
ld -o build/02 build/02.o
./build/02
```

Or, for debugging:

```
nasm -g -F dwarf -f elf64 src/2023/02/02.asm -o build/02.o
ld -o build/02 build/02.o
gdb -tui build/02
```

in GDB:

```
tui reg general
b _start
r
s
b 02.asm:13
c
```

etc.